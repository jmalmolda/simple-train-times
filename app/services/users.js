(function(){
    
    var users = function($http){
        var getUser = function() {
            return $http.get('/api/user')
                    .then(function(response) {
                        return response.data;
                    });
        };
        
        var onMessageSent = function() {
          console.log('message has been sent');  
        };
        
        var onMessageSentError = function(err) {
          console.log('there was an error sending the message:' + err);  
        };
        
        var sendMessage = function(userEmail, userName, clientName, clientEmail, emailText)         {
            return $http.post('/api/sendMessage',
                            {
                                userEmail: userEmail,
                                userName: userName,
                                clientName: clientName,
                                clientEmail: clientEmail,
                                emailText: emailText
                            })
                        .then(onMessageSent, onMessageSentError);
        };
    
        return {
            getUser: getUser,
            sendMessage: sendMessage
        };
        
    };
    
    var module = angular.module("artSite");
    module.factory("users", users);
}());