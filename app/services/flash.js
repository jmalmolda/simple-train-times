(function(){
    var flash = function($timeout) {
      var currentMessage = "";

      return {
        setMessage: function(message) {
            console.log('message set:' + message);
            currentMessage = message;
            $timeout(function() {currentMessage = ''}, 5000);
        },
        getMessage: function() {
            return currentMessage;
        }
      };
    };
    var module = angular.module("tfl");
    module.factory("flash", flash);
}());
