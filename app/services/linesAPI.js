(function(){
    
    var linesAPI = function($http){
        
        var getAllLines = function() {
            var lines = ["northern", "victoria", "bakerloo", "waterloo-city"];
            return lines;
        };
        
        var getStops = function(lineName) {
            return $http.get("https://api.tfl.gov.uk/Line/" + lineName + "/StopPoints")
            .then(function(response) {
                return response.data;
            });
        };
        
        var getTimes = function(stopId, lineName) {            
            return $http.get("https://api.tfl.gov.uk/Line/" + lineName + "/Arrivals?stopPointId=" + stopId)
            .then(function(response) {
                var trainTimes = [];
                for (var i = 0; i < response.data.length; i++) {
                    trainTimes.push(new TrainTime(response.data[i]));
                }
                trainTimes.sort(function(a, b) {
                    return a.calculateMinutes() - b.calculateMinutes();
                });
                return trainTimes;
            });
        }
        
        
        return {
            getAllLines: getAllLines,
            getStops: getStops,
            getTimes: getTimes
        };
        
    };
    
    var module = angular.module("tfl");
    module.factory("linesAPI", linesAPI);
}());