(function(){
    var projectMessage = function() {
      var title = "";
      var gallery = "";

      return {
        setMessage: function(projectTitle, projectGallery) {
            title = projectTitle;
            gallery = projectGallery;
        },
        getMessage: function() {
            return title && gallery ? 'I am interested in painting titled "' + title + '" from the "' + gallery + '" gallery' : '';
        },
        deleteMessage: function() {
            title = gallery = '';
        }
      };
    };
    var module = angular.module("artSite");
    module.factory("projectMessage", projectMessage);
}());