(function() {

    var app = angular.module("tfl");

    var ContactUsController = function($scope, flash, $location) {
        $scope.sendMessage = function() {
            flash.setMessage("message sent");
            $location.path('/home');
        };
    };
    app.controller("ContactUsController", ContactUsController);

}());