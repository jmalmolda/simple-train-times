(function(){
    
    var app = angular.module("tfl", ["ngRoute", "ngDialog"]);
    
    app.config(function($routeProvider){
        $routeProvider
            .when("/home", {
                templateUrl: "app/views/home.html",
                controller: "HomeController"
            })
            .when("/line/:lineName", {
                templateUrl: "app/views/line.html",
                controller: "LineController" 
            })
            .when("/contactus", {
                templateUrl: "app/views/contactus.html",
                controller: "ContactUsController" 
            })
            .otherwise({redirectTo:"/home"});
    });
}());