(function() {

    return TrainTime = function(trainTimeData) {
                this.platform = trainTimeData.platformName;
                this.direction = trainTimeData.towards;
                this.calculateMinutes = function() {
                    return trainTimeData.timeToStation / 60 >> 0;
                };
            };

}());