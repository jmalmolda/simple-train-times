(function() {

    var app = angular.module("tfl");

    var LineController = function($scope,$routeParams,linesAPI) {

        var lineName = $routeParams.lineName;
        $scope.lineName = lineName;
        $scope.show = false;
        
        linesAPI.getStops(lineName)
            .then(function(result) {
                $scope.stops = result;
            });
        
        $scope.update = function() {
            linesAPI.getTimes($scope.selectedStop.id, lineName)
                .then(function(result) {
                    $scope.trains = result;
                    $scope.show = true;
                });
        }
    };

    app.controller("LineController", LineController);

}());