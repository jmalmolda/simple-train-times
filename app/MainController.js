(function() {

    var app = angular.module("tfl");

    var MainController = function($scope, $http, linesAPI, $location, flash) {
        $scope.location = $location;
        $scope.header = "TFL times app";
        $scope.subheader = "check train times by stations";
        $scope.lines = linesAPI.getAllLines();
        $scope.flash = flash;
        
        var updatedTimes = 0;
        $scope.updateTitle = function() {
            updatedTimes++;
            $scope.header = "updated times: " + updatedTimes;
        };
    };

    app.controller("MainController", MainController);

}());